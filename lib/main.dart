// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import '/src/view/MyHomePage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // root application widget
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter MVC Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}
